## [springboot官方文档](http://spring.io/projects/spring-boot)
## [本项目地址](https://gitee.com/springbook_foundation/springboot-base-two)

### 基础maven包
1. spring-boot-starter-parent springboot 启动父包
2. spring-boot-starter-web 开启web服务
3. spring-boot-starter-test 测试类包
4. spring-boot-maven-plugin plugin插件 能够将Spring Boot应用打包为可执行的jar或war文件
5. mybatis-spring-boot-starter  mybatis开发包
6. mysql-connector-java mysql开发包
7. druid 阿里数据库连接池开发包

### springBoot项目 目录结构


```bash
│ pom.xml
└─src
    └─main
        ├─java
        │  └─com
        │      └─base
        │          │  Application.java ## springboot启动类
        │          │
        │          ├─config
        │          │      WebMvcConfig.java ## springmvc控制，过滤资源目录
        │          │
        │          ├─controller ## controller控制层
        │          │      IndexController.java
        │          │ 
        │          ├─domain ## 实体类
        │          │      SysUserInfo.java
        │          │
        │          └─mapper ## mapper接口
        │                  SysUserInfoMapper.java
        │
        └─resources 
            │  application.yml ## springboot配置文件
            │
            ├─mappers ## sql文件 xml
            │      SysUserInfoMapper.xml
            │
            └─mybatis ## mybatis配置
                    mybatis-config.xml
```

###  [mybatis-config.xml详解（mybatis配置文件）](http://www.mybatis.org/mybatis-3/zh/configuration.html#settings)

### [XML 映射文件详解（mappers.xml）](http://www.mybatis.org/mybatis-3/zh/sqlmap-xml.html#select)

### [动态sql(if,foreach) 等](http://www.mybatis.org/mybatis-3/zh/dynamic-sql.html#)



### pom.xml配置

```
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>springboot-base</groupId>
    <artifactId>springboot-base-two</artifactId>
    <version>1.0-SNAPSHOT</version>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>1.5.10.RELEASE</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>

    <properties>
        <java.version>1.8</java.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <!-- Build args -->
        <argline>-server -Xms64m -Xmx256m -XX:PermSize=64m -XX:MaxPermSize=128m -Dfile.encoding=UTF-8
            -Djava.net.preferIPv4Stack=true
        </argline>
    </properties>

    <repositories>
        <repository>
            <id>aliyun-releases</id>
            <url>http://maven.aliyun.com/nexus/content/groups/public</url>
        </repository>
    </repositories>

    <!-- 设定插件仓库 -->
    <pluginRepositories>
        <pluginRepository>
            <id>aliyun-repos</id>
            <url>http://maven.aliyun.com/nexus/content/groups/public</url>
        </pluginRepository>
    </pluginRepositories>

    <dependencies>
        <!--开启web服务-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <!--测试类包-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>

        <!--mybatis-->
        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
            <version>1.3.2</version>
        </dependency>

        <!--mysql-->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.21</version>
        </dependency>
        <!--druid数据库连接池-->
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid</artifactId>
            <version>1.0.27</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
</project>
```

### application.yml 配置

```
spring:
  application:
    # 服务名
    name: basse
  datasource:
    # 数据库地址
    url: jdbc:mysql://123.206.19.217:3306/springboot?useUnicode=true&amp;characterEncoding=UTF-8&amp;tinyInt1isBit=false
    # 数据库账号
    username: root
    # 数据库密码
    password: lzq199528-d
    # Driver驱动
    driver-class-name: com.mysql.jdbc.Driver
    # DruidDataSource 阿里数据库连接池
    type: com.alibaba.druid.pool.DruidDataSource
    #连接池的配置信息
    # 原始大小
    initialSize: 5
    # 最小空闲数
    minIdle: 5
    # 最大连接数据库连接数
    maxActive: 20
    # 最大等待毫秒数
    maxWait: 60000
    timeBetweenEvictionRunsMillis: 60000
    # 最小检测时间毫秒
    minEvictableIdleTimeMillis: 300000
    # 验证查询
    validationQuery: SELECT 1 FROM DUAL
    testWhileIdle: true
    testOnBorrow: false
    testOnReturn: false
    poolPreparedStatements: true
    maxPoolPreparedStatementPerConnectionSize: 20
    filters: stat,wall,log4j
    connectionProperties: druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000
    useGlobalDataSourceStat: true

server:
 # 服务端口
  port: 8002

#mybatis 配置信息 代码配置
mybatis:
  # 实体类所在目录
  type-aliases-package: com.base.domain
  # mybatis配置文件
  config-location: classpath:/mybatis/mybatis-config.xml
  # mapper xml文件位置 数组类型 可以写一个也可以写两个
  mapperLocations: classpath:/mappers/**/*.xml,classpath:/mappers/*.xml
```

### mybatis-config.xml配置

```
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration PUBLIC "-//mybatis.org//DTD Config 3.0//EN" "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <!-- 配置mybatis的缓存，驼峰映射 -->
    <settings>
        <setting name="mapUnderscoreToCamelCase" value="true"/>
        <!--  全局地开启或关闭配置文件中的所有映射器已经配置的任何缓存。  	-->
        <setting name="cacheEnabled" value="true"/>
    </settings>
</configuration>
```


