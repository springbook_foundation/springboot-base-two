package com.base.domain;


/**
 * 开发公司：个人
 * 版权：个人
 * <p>
 * SysUserInfo
 *
 * @author 刘志强
 * @created Create Time: 2019/5/23
 */
public class SysUserInfo {

    private Long id;

    private String userName;

    private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}