package com.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 开发公司：个人
 * 版权：个人
 * <p>
 * Application
 * @SpringBootApplication(scanBasePackages = {"com.base"})  scanBasePackages 扫描基础类包
 * @author 刘志强
 * @created Create Time: 2019/5/23
 */
@SpringBootApplication(scanBasePackages = {"com.base"})
@EnableAutoConfiguration
public class Application {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}