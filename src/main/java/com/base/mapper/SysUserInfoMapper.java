package com.base.mapper;

import com.base.domain.SysUserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


/**
 * 开发公司：个人
 * 版权：个人
 * <p>
 * SysUserInfoMapper
 *
 * @author 刘志强
 * @created Create Time: 2019/5/23
 */
@Mapper
public interface SysUserInfoMapper {

    SysUserInfo getSysUserInfo(@Param("id") Long id);

    SysUserInfo getSysUser(@Param("id") Long id);
}