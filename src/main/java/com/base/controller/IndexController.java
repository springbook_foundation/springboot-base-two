package com.base.controller;

import com.base.domain.SysUserInfo;
import com.base.mapper.SysUserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 开发公司：个人
 * 版权：个人
 * <p>
 * IndexController
 *
 * @author 刘志强
 * @created Create Time: 2019/5/23
 */
@RestController
public class IndexController {

    @Autowired
    public SysUserInfoMapper sysUserInfoMapper;
    // 重定向至首页
    @GetMapping("/")
    public void root(HttpServletResponse httpServletResponse) throws IOException {
        httpServletResponse.sendRedirect("/index");
    }

    @GetMapping("/index")
    @ResponseBody
    public Map index() {
        Map<String,Object> map = new HashMap<>();
        // 简单查询
        SysUserInfo sysUserInfo = sysUserInfoMapper.getSysUserInfo(new Long("1"));
        SysUserInfo userInfo = sysUserInfoMapper.getSysUser(new Long("1"));
        map.put("sysUserInfo",sysUserInfo);
        map.put("userInfo",userInfo);
        return map;
    }
}